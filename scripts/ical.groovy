
@Grab(group='org.mnode.ical4j', module='ical4j', version='3.0.21')

import net.fortuna.ical4j.model.*
import net.fortuna.ical4j.model.property.DtStamp

currentyear = args.length > 0 ? args[0] as int : new Date()[java.util.Calendar.YEAR]

[
    'es',
    'gal',
    'astu',
    'eus',
    'cat',
    'arag',
    'en',
    'epo',
    'fra',
    'arab'
].each{ lang ->
    if( new File("source/csv/${currentyear}/${lang}.tsv").exists() ){
        new File("source/csv/$currentyear/${lang}.tsv").withReader{ r->

            r.readLine()

            def builder = new ContentBuilder()
            def calendar = builder.calendar() {
                prodid '-//Ben Fortuna//iCal4j 1.0//EN'
                version '2.0'
                def line
                while( (line=r.readLine())!= null){
                    def fields = line.split('\t')
                    def title = fields[4].split('\\.').first()
                    vevent {                
                        uid String.format('%04d%02d%02d-%s', fields[2] as int, fields[1] as int, fields[0] as int, lang)
                        dtstamp new DtStamp()
                        dtstart String.format('%04d%02d%02d', fields[2] as int, fields[1] as int, fields[0] as int), parameters: parameters {
                            value('DATE')
                        }
                        summary title
                        description fields[4]
                        action 'DISPLAY'
                    }
                }
            }
            def file = new File("docs/${lang}/modules/ROOT/attachments/${lang}.ical")
            file.parentFile.mkdirs()
            file.text = calendar.toString()   
        }
    }
}