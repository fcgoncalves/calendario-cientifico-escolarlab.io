= Un aniversariu científicu pa caldia l’añu

El Calendariu Científicu Escolar diríxese principalmente al alumnáu d’educación primaria y secundaria obligatoria. Caldía recuéyese un aniversariu científicu o teunolóxicu como, por exemplu, ñacimientu de persones d’estes estayes o cabu d’años d’afayamientos destacaos.

El Calendariu acompáñase d’una guía didáutica con orientaciones pal so provechu educativu transversal nes clases. Estes propuestes didáutiques entamen de los principios d’inclusión, normalización ya equiadá ya acompáñense de pautes xenerales d’accesibilidá. N’esti sen, proporciónense tarees variaes qu'inclúin una bayura d’habilidaes ya estayes de dificultá y que, desendolcaes a mou cooperativu, dexen que tou l’alumnau faiga aportaciones útiles y relevantes.

Por mor al so espardimientu ya emplegu nes aules, toos los materiales tórnense a delles llingües: castellán, gallegu, euskera, catalán, asturianu, aragonés, inglés, francés, esperantu ya árabe.

La información colos aniversarios diarios y les semeyes que los sofiten dispónense de baldre n’esti repositoriu. Amás, pue descargase el calendariu y la guía (maquetáos en pdf y testu planu) dende la páxina web del IGM (http://www.igm.ule-csic.es/calendario-cientifico).

Esta iniciativa procura caltener la cultura científica na población más mozo y crear referentes lo más averaos a ellos. Por ello, faese un esfuerciu mayor en dar a conocer persones ya afayamientos d’anguañu que constituín referencies pa la xente mozo, y al mesmu tiempu, amosar dinamismu y actualidá. Emprestóse especial atención al fomentu d'un llinguaxe non sexista y al aumentu de la visibilidá de les muyeres científiques y teunólogues, pa poner a disposición modelos referentes qu’algamen les vocaciones científicu-técniques ente les neñes y adolescentes. Tamién púnxose un enfotu particular en sopelexar l'actividá investigadora de los centros públicos españoles.

== Twitter

@CalCientifico

== Telegram

https://t.me/CalendarioCientifico

== iCal

link:{attachmentsdir}/astu.ical[Download]
